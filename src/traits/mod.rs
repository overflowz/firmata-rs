use crate::enums::PinMode;
use crate::firmata::Pin;
use std::io::Result;

pub trait Firmata {
  fn pins(&self) -> &Vec<Pin>;
  fn protocol_version(&self) -> &String;
  fn firmware_name(&self) -> &String;
  fn firmware_version(&self) -> &String;
  fn reset(&mut self) -> Result<()>;
  fn query_firmware(&mut self) -> Result<()>;
  fn query_capabilities(&mut self) -> Result<()>;
  fn query_analog_mapping(&mut self) -> Result<()>;
  fn report_digital(&mut self, pin: u8, state: u8) -> Result<()>;
  fn report_analog(&mut self, pin: u8, state: u8) -> Result<()>;
  fn set_pin_mode(&mut self, pin: u8, mode: PinMode) -> Result<()>;
  fn digital_write(&mut self, pin: u8, level: u8) -> Result<()>;
}
