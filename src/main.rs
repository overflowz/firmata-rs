extern crate firmata_rs;

use firmata_rs::traits::Firmata;
use firmata_rs::*;
use serial::SerialPort;

fn main() {
  let mut sp = serial::open("COM5").unwrap();

  sp.reconfigure(&|settings| {
    settings.set_baud_rate(serial::Baud57600).unwrap();
    settings.set_char_size(serial::Bits8);
    settings.set_parity(serial::ParityNone);
    settings.set_stop_bits(serial::Stop1);
    settings.set_flow_control(serial::FlowNone);
    Ok(())
  })
  .unwrap();

  let mut board = firmata::Board::new(Box::new(sp)).unwrap();
  board
    .set_pin_mode(13, firmata_rs::enums::PinMode::Output)
    .unwrap();

  board
    .set_pin_mode(12, firmata_rs::enums::PinMode::Input)
    .unwrap();

  loop {
    println!("{:?}", board.pins()[12].value);
    std::thread::sleep(std::time::Duration::from_secs(1));
  }

  // board.report_digital(13, 1).unwrap();
  // board.digital_write(13, 1).unwrap();
  // board.digital_write(13, 0).unwrap();

  println!("{:?}", board.firmware_name());
  println!("{:?}", board.firmware_version());
  println!("{:?}", board.protocol_version());
}
