#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PinMode {
  Input = 0x00,
  Output = 0x01,
  Analog = 0x02,
  PWM = 0x03,
  Servo = 0x04,
  Shift = 0x05,
  I2C = 0x06,
  OneWire = 0x07,
  Stepper = 0x08,
  Serial = 0x0A,
  Pullup = 0x0B,
  Ignore = 0x7F,
  PingRead = 0x75,
  Unknown = 0x10,
}

impl From<u8> for PinMode {
  fn from(v: u8) -> PinMode {
    match v {
      0x00 => PinMode::Input,
      0x01 => PinMode::Output,
      0x02 => PinMode::Analog,
      0x03 => PinMode::PWM,
      0x04 => PinMode::Servo,
      0x05 => PinMode::Shift,
      0x06 => PinMode::I2C,
      0x07 => PinMode::OneWire,
      0x08 => PinMode::Stepper,
      0x0A => PinMode::Serial,
      0x0B => PinMode::Pullup,
      0x7F => PinMode::Ignore,
      0x75 => PinMode::PingRead,
      0x10 => PinMode::Unknown,
      _ => PinMode::Unknown,
    }
  }
}
