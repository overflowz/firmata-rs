extern crate encoding_rs;
extern crate serial;

pub mod constants;
pub mod enums;
pub mod traits;

pub mod firmata {
  use crate::constants;
  use crate::enums;
  use crate::traits;

  use crate::traits::Firmata;

  use encoding_rs;
  use std::io::{Error, ErrorKind, Read, Result, Write};
  use std::thread;
  use std::time::Duration;

  #[derive(Debug, Clone)]
  pub struct Mode {
    pub mode: enums::PinMode,
    pub resolution: u8,
  }

  #[derive(Debug, Clone)]
  pub struct Pin {
    pub supported_modes: Vec<Mode>,
    pub analog: bool,
    pub value: u8,
    pub mode: enums::PinMode,
    pub report: bool,
  }

  #[derive(Debug)]
  pub struct Board<T: Read + Write> {
    sp: Box<T>,
    pins: Vec<Pin>,
    protocol_version: String,
    firmware_name: String,
    firmware_version: String,
  }

  impl<T: Read + Write> Board<T> {
    pub fn new(sp: Box<T>) -> Result<Board<T>> {
      let mut board = Board {
        sp,
        pins: vec![],
        protocol_version: String::new(),
        firmware_version: String::new(),
        firmware_name: String::new(),
      };

      board.reset()?;
      thread::sleep(Duration::from_millis(5));

      board.query_firmware()?;
      thread::sleep(Duration::from_millis(5));

      board.query_capabilities()?;
      thread::sleep(Duration::from_millis(5));

      board.query_analog_mapping()?;
      thread::sleep(Duration::from_millis(5));

      Ok(board)
    }

    fn read(&mut self, len: u32) -> Result<Vec<u8>> {
      let mut vec: Vec<u8> = vec![];
      let mut len = len;
      let mut timeout_retries = 0;

      loop {
        let buf: &mut [u8; 1] = &mut [0u8];

        match self.sp.read(buf) {
          Ok(_) => {
            vec.push(buf[0]);

            len = len - 1;
            if len == 0 {
              break;
            }
          }
          Err(e) => {
            if e.kind() == ErrorKind::TimedOut {
              thread::sleep(Duration::from_millis(1));
              timeout_retries += 1;

              if timeout_retries > 10 {
                return Ok(vec);
              }

              continue;
            }
          }
        }
      }

      Ok(vec)
    }

    fn read_and_decode(&mut self) -> Result<()> {
      let mut buf = self.read(1)?;
      if buf.len() == 0 {
        return Ok(());
      }

      match buf[0] {
        constants::REPORT_VERSION => {
          let version = self.read(2)?;
          self.protocol_version = format!("{:o}.{:o}", version[0], version[1]);

          // sysex messages are coming afterwards.
          self.read_and_decode()
        }
        constants::ANALOG_MESSAGE...0xEF => {
          let msg = self.read(2)?;
          let pin: u8 = buf[0];
          let value: u8 = (msg[0] | msg[1] << 7) as u8;

          if self.pins().len() > pin as usize {
            self.pins[pin as usize].value = value;
          }

          Ok(())
        }
        constants::DIGITAL_MESSAGE...0x9F => {
          let msg = self.read(2)?;
          let port: u8 = (buf[0] & 0x0F) as u8;
          let value: u8 = (msg[0] | msg[1] << 7) as u8;

          for i in 0..8 {
            let pin = (8 * port) + i;

            if self.pins.len() > pin as usize {
              let pin_ref = &mut self.pins[pin as usize];

              if pin_ref.mode == enums::PinMode::Input || pin_ref.mode == enums::PinMode::Pullup {
                pin_ref.value = (value >> (i & 0x07)) & 0x01;
              }
            }
          }

          Ok(())
        }
        constants::START_SYSEX => {
          loop {
            let message = self.read(1)?;
            buf.push(message[0]);

            if message[0] == constants::END_SYSEX {
              break;
            }
          }

          match buf[1] {
            constants::QUERY_FIRMWARE => {
              self.firmware_version = format!("{:o}.{:o}", buf[2], buf[3]);
              self.firmware_name = encoding_rs::UTF_16LE
                .decode(&buf[4..buf.len() - 1])
                .0
                .to_string();

              Ok(())
            }
            constants::CAPABILITY_RESPONSE => {
              // TODO: not sure.. shows 21 pins while there are 20 currently.
              let mut pin = 0;
              let mut i = 2;

              self.pins = vec![];
              self.pins.push(Pin {
                supported_modes: vec![],
                analog: false,
                value: 0,
                report: true,
                mode: enums::PinMode::Unknown,
              });

              while i < buf.len() - 1 {
                if buf[i] == 0x7F {
                  pin += 1;
                  i += 1;

                  self.pins.push(Pin {
                    supported_modes: vec![],
                    analog: false,
                    value: 0,
                    report: true,
                    mode: buf[i].into(),
                  });
                } else {
                  self.pins[pin as usize].supported_modes.push(Mode {
                    mode: buf[i].into(),
                    resolution: buf[i + 1],
                  });

                  i += 2;
                }
              }

              Ok(())
            }
            constants::ANALOG_MAPPING_RESPONSE => {
              if self.pins.len() == 0 {
                return Ok(());
              }

              let mut i = 2;
              while i < buf.len() - 1 {
                if buf[i] != 0x7F {
                  self.pins[i - 2].analog = true;
                }

                i += 1;
              }

              Ok(())
            }
            _ => Err(Error::new(
              ErrorKind::Other,
              format!("Unknown sysex command: 0x{:X}", buf[1]),
            )),
          }
        }
        _ => Err(Error::new(
          ErrorKind::Other,
          format!("Bad byte: 0x{:X}", buf[0]),
        )),
      }
    }
  }

  impl<T: Read + Write> traits::Firmata for Board<T> {
    fn pins(&self) -> &Vec<Pin> {
      &self.pins
    }

    fn protocol_version(&self) -> &String {
      &self.protocol_version
    }

    fn firmware_name(&self) -> &String {
      &self.firmware_name
    }

    fn firmware_version(&self) -> &String {
      &self.firmware_version
    }

    fn reset(&mut self) -> Result<()> {
      self
        .sp
        .write(&mut [constants::SYSTEM_RESET])
        .and_then(|_| Ok(()))
    }

    fn query_firmware(&mut self) -> Result<()> {
      self
        .sp
        .write(&mut [
          constants::START_SYSEX,
          constants::QUERY_FIRMWARE,
          constants::END_SYSEX,
        ])
        .and_then(|_| self.read_and_decode())
    }

    fn query_capabilities(&mut self) -> Result<()> {
      self
        .sp
        .write(&mut [
          constants::START_SYSEX,
          constants::CAPABILITY_QUERY,
          constants::END_SYSEX,
        ])
        .and_then(|_| self.read_and_decode())
    }

    fn query_analog_mapping(&mut self) -> Result<()> {
      self
        .sp
        .write(&mut [
          constants::START_SYSEX,
          constants::ANALOG_MAPPING_QUERY,
          constants::END_SYSEX,
        ])
        .and_then(|_| self.read_and_decode())
    }

    fn report_digital(&mut self, pin: u8, state: u8) -> Result<()> {
      self
        .sp
        .write(&mut [constants::REPORT_DIGITAL | pin, state])
        .and_then(|_| Ok(()))
    }

    fn report_analog(&mut self, pin: u8, state: u8) -> Result<()> {
      self
        .sp
        .write(&mut [constants::REPORT_ANALOG | pin, state])
        .and_then(|_| Ok(()))
    }

    fn set_pin_mode(&mut self, pin: u8, mode: enums::PinMode) -> Result<()> {
      if self.pins.len() < pin as usize {
        return Err(Error::new(
          ErrorKind::Other,
          format!("Invalid pin: {:?}", pin),
        ));
      }

      self.pins[pin as usize].mode = mode;

      self
        .sp
        .write(&mut [constants::PIN_MODE, pin, mode as u8])
        .and_then(|_| Ok(()))
    }

    fn digital_write(&mut self, pin: u8, level: u8) -> Result<()> {
      if self.pins.len() < pin as usize {
        return Err(Error::new(
          ErrorKind::Other,
          format!("Invalid pin: {:?}", pin),
        ));
      }

      let port: u8 = pin >> 3;
      let mut value = 0u8;

      self.pins[pin as usize].value = level;

      for i in 0..8 {
        if self.pins[8 * port as usize + i].value != 0 {
          value |= 1 << i;
        } else {
          value &= !(1 << i);
        }
      }

      self
        .sp
        .write(&mut [
          constants::DIGITAL_MESSAGE | port as u8,
          value & 0x7F,
          (value >> 7) & 0x7F,
        ])
        .and_then(|_| self.read_and_decode())
    }
  }
}
